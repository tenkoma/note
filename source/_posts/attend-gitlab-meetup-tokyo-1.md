---
title: GitLab Meetup Tokyo &#x23;1 に参加しました
date: 2017/3/5
---

* [GitLab Meetup Tokyo #1 - connpass](https://gitlab-jp.connpass.com/event/49755/)
* [GitLab Meetup Tokyo #1 #gitlabjp - Togetterまとめ](https://togetter.com/li/1086484)

2017/03/02(木) に[ピクシブ株式会社](http://www.pixiv.co.jp/)オフィス(千駄ヶ谷)で行われた[GitLab Meetup Tokyo #1](https://gitlab-jp.connpass.com/event/49755/)に参加しました。ブログ絶対書く枠で参加したので GitLab Pages でレポート公開します！

## 参加のモチベーション

* 業務用のプライベートな Git リポジトリとして [GitLab](https://about.gitlab.com/) を使っていて、[社内の技術情報共有でも使っている](http://bashalog.c-brains.jp/16/06/06-101010.php)
* 制作フローの省力化、自動化のため GitLab CI を試していて、ノウハウを集めたかった。 (例:[GitLabでPushされたときにESLintやstylelintを実行する | バシャログ。](http://bashalog.c-brains.jp/17/01/10-202806.php)) PHPアプリのビルド、デプロイもやりたい。
* gitlab-ce でも GitLab Pages が使えるようになったので、活用したい。

### @tnir [開発統合プラットフォームとしてのGitLabと今後の展開 - Qiita](http://qiita.com/tnir/items/ebfb2d8c04bcda6491cb)

GitLab の歴史やプロダクトの位置づけ、最近のアップデートで追加された機能などを俯瞰的に紹介。基調講演的な位置づけでしょうか。
[GitLab.comデータベース消失事故](http://www.publickey1.jp/blog/17/gitlabcom56.html)がもさらっと紹介。GitLabという製品に問題があったのではなく、オペミス。 **←勘違いされそうなので重要**

* [GitHost | Automated GitLab and CI Hosting](https://githost.io/) …GitLab ホスティングサービス、知らなかった
* インストールが大変だったのも今や昔の話。yum/apt でインストール・アップデート可能
* [2回目も開催が決まってたw](https://gitlab-jp.connpass.com/event/52276/) 紹介時点ですでに40人が埋まっていて滑り込みで参加登録しました。

### @catatsuy [GitLabの実践的な運用 #gitlabjp - Qiita](http://qiita.com/catatsuy/items/b7fda84ca9cb3dbcdc79)

GitLab のインストール、MySQLを使うときの注意、バックアップ、アップグレードなど。

* グローバルIPを持つサーバが必要
* サーバは1台が前提
* DBサーバは別に出来る
* GitLabのデータ種別

など豆知識として覚えておきたい。GitLab をMySQLで使うのは考えないことに…

### @hiroponz [GitLabコミュニティーへのコントリビュート - Google スライド](https://docs.google.com/presentation/d/1K6Ws7hHDdgiIlBKTtM6aB7tzasp5pE-yQPpX-ylKYDY/edit#slide=id.p)

GitLab に MR を送るときに知っておきたい知識まとめ。コントリビュートのメリットやマージされやすくなるポイントについては GitLab 特有の話ではないと思いました。ここ半年くらい [CakePHP](https://github.com/cakephp/)プロジェクト(主にドキュメント)に PR を送るようになったので共感できることが多かった。
GitLab も日常的に使っているツールなので、コントリビュートできるかも…？

### @sue445 [ここがすごいよGitLab CI #gitlabjp](https://sue445.gitlab.io/gitlab-meetup-tokyo-1/#/)

* **Twitterフォロー~~してしまった~~しました**
* [GitLab Notifier for Google Chrome™ (a.k.a Chrome GitLab Notifier) - Qiita](http://qiita.com/sue445/items/630003a5d617a4ea17bc) 便利そう
* GitLab-CI は [ESLintやstylelintを実行する](http://bashalog.c-brains.jp/17/01/10-202806.php) 記事で導入方法をまとめたのですが、 gitlab-ci-multi-runner も yum でインストールできて確かに楽。登録すれば Admin Area でも追加されてることが分かるので。
* GitLab CI が向いてない事例

CI 設定は yaml ファイルをコピペすれば使い回せるのはよいですね
GitLab CI は docker runner を使えば Docker の勉強にもなるのもよいと思った

### LT 1: @uchienneo 「GitLabを利用したプロジェクト運営(仮)」

GitLab の カンバンボードライクな機能 (Board, Milestone) の活用について。
個人ツール作成では使ってますがチーム開発では Redmine なので、活用事例として参考になる。
全体俯瞰とドラッグアンドドロップ出来るところがいいですね。あとは、レーン内で順番入れ替えが出来れば…

### LT 2: @jvasseur [GitLab CI with docker-in-docker](https://www.slideshare.net/secret/oS6viMY5jWmmuF)

LT 聞いたときはあまり分からなかったので資料をチェックしての今の理解

* CI マシンを複数のプロジェクトで使う、同時実行が想定される→ Docker runner でええやん
* GitLab CI で Docker イメージをビルドする→ Docker in Docker (DIND) を使う
* DIND でのビルドはいくつか問題が → Shell runner を使ってみる
  * Shell runner もあまり推奨できない(セキュリティとか)
* Docker 1.13 の `--cache-from` オプションを使うと キャッシュ、同時並行性などの問題が解決
  

### LT 3: @Kirika_K2 [ファイルシステムのスナップショット機能でバックアップを取得する](https://www.slideshare.net/MakiToshio/ss-72735571)

* LVM の話

LVM 概要はわかりますが、コマンドで動作確認したことはないと思う
リストア時は無停止ではできないが、リストア方法さえ把握していれば手軽かも…？

### LT 5: @jeffi7 [「わりと大きい会社でGitLabをホスティングしてみた話」](https://www.slideshare.net/inosuke/gitlab-meetup-tokyo-1-lt-gitlab)

* プロプライエタリなツール群から GitLab に移行した話
* Rails の知見がない、どうする？
* Docker コンテナがリリースされてた
* ユーザー/プロジェクト数 1,000 over
* Prometheus 入門中

## まとめ

CI/CD の話が知りたかったのが目的で参加しましたが、コントリビュートや運用上の問題の話も今後の役に立ちそうです。
Gitリポジトリをホストすること自体が業務ではないですが、セルフ運用するからには知識経験はあればあるほどよいので、楽しく使えるようになりたいですね。

## おまけ: 参加レポートのために GitLab Pages を使ってみました

### 静的サイトジェネレータ

せっかくなので、GitLab Pages で参加レポートを公開してみよう、ということで 3 時間ほどかけて環境構築しました。静的コンテンツジェネレータはまともに使ったことがないので、ツール選びの必要があり、以下のツールを選択。(ツールの善し悪しは検討せず、サイトの見た目で選択しました)

* [Hexo](https://hexo.io/) JavaScript 製の静的 Blog ジェネレータ
* [MikeCoder/hexo-theme-gandalfr: Yet, just another blog theme for Hexo, based on hexo-theme-apollo.](https://github.com/MikeCoder/hexo-theme-Gandalfr) 配色が Vue.js っぽい感じのシンプルな Hexo テーマ

### GitLab CI でGitLab Pages に公開

このブログは GitLab Pages でホストされていますが、初めて使いました。
最近 GitLab CI + docker runner で少し遊んでいるので、なんとかなると思いました。
`.gitlab-ci.yml` の設定は以下の通りです。
(キャッシュ設定などサイト公開に直接不要な項目を削除)

```yaml
image: node:7.7
before_script:
  - yarn install

stages:
  - deploy

pages:
  stage: deploy
  script:
    - yarn deploy
  artifacts:
    paths:
      - public
```
完全な設定は [.gitlab-ci.yml · master · Koji Tanaka / note · GitLab](https://gitlab.com/tenkoma/note/blob/master/.gitlab-ci.yml) に

`pages:` セクションで `package.json` に定義したスクリプトを実行して、 `public` ディレクトリに生成。 `artifacts:` に `public` を指定すると、それが GitLab Pagesで公開される仕組みのようです。
GitLab Pages のために、ブラウザから GitLab リポジトリの設定を変更する必要はありません。
Git リポジトリへの変更がすべてなので、ノウハウを共有したいときにありがたいと思いました。

コミット履歴は[Commits · master · Koji Tanaka / note · GitLab](https://gitlab.com/tenkoma/note/commits/master)
