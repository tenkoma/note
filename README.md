# Git Repository for [tenkoma's GitLab note](https://tenkoma.gitlab.io/note/)

# Powered by

* [Hexo](https://hexo.io/)
* [MikeCoder/hexo-theme-gandalfr](https://github.com/MikeCoder/hexo-theme-Gandalfr)

## install

```bash
git clone https://gitlab.com/tenkoma/note.git tenkoma-gitlab-note
cd tenkoma-gitlab-note
npm install -g yarn
yarn install
```

## run local server

```bash
yarn server
```

## generate html

```bash
yarn deploy
```